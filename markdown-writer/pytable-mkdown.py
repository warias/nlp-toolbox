import json
from pytablewriter import MarkdownTableWriter

report = json.load(open('report.json'), encoding= 'UTF-8')
issues = [issue for issue in report]
columns_keys = ['project_id', 'title', 'labels','created_at']

all_issues = []
for index in range(0, len(issues)):
    issue_values = [issues[index][column_key] for column_key in columns_keys]
    all_issues.append(issue_values)

writer = MarkdownTableWriter()
writer.table_name = "ARM Compatibility Report "
writer.headers = columns_keys
writer.value_matrix = [issue_values for issue_values in all_issues]
writer.dump('arm_report.md')